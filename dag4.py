# -*- coding: utf-8 -*-
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.

"""
### Tutorial Documentation
Documentation that goes along with the Airflow tutorial located
[here](https://airflow.incubator.apache.org/tutorial.html)
"""
from datetime import timedelta
from airflow.operators.python_operator import PythonOperator
import airflow
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from jsontomongo import JsonToMongoOperator


# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'Samir',
    'depends_on_past': False,
    'start_date': airflow.utils.dates.days_ago(2),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(days=1),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    # 'wait_for_downstream': False,
    # 'dag': dag,
    # 'adhoc':False,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function,
    # 'on_success_callback': some_other_function,
    # 'on_retry_callback': another_function,
    # 'trigger_rule': u'all_success'
}

dag = DAG(
    'python4',
    default_args=default_args,
    description='Python script',
    schedule_interval=timedelta(days=1),
)

# t1, t2 and t3 are examples of tasks created by instantiating operators
def getjson(ds, **kwargs):
    import requests,pymongo, datetime,json   

    myapikey="a2ab3a1da8ff668e00a13aa228dab20b"

    profile=requests.get(f"https://financialmodelingprep.com/api/v3/profile/AAPL?apikey={myapikey}")

    profile=profile.json()[0]


    rating=requests.get(f"https://financialmodelingprep.com/api/v3/rating/AAPL?apikey=a2ab3a1da8ff668e00a13aa228dab20b")

    rating=rating.json()[0]

    data={"profile":profile,"current timestamp":datetime.datetime.now().timestamp()}

    with open('/tmp/p.json', 'w') as cred:
        json.dump(data, cred)


def getr(ds, **kwargs):
    import requests,pymongo, datetime,json   

    myapikey="a2ab3a1da8ff668e00a13aa228dab20b"

    profile=requests.get(f"https://financialmodelingprep.com/api/v3/profile/AAPL?apikey={myapikey}")

    profile=profile.json()[0]


    rating=requests.get(f"https://financialmodelingprep.com/api/v3/rating/AAPL?apikey=a2ab3a1da8ff668e00a13aa228dab20b")

    rating=rating.json()[0]

    data={"rating":rating,"current timestamp":datetime.datetime.now().timestamp()}

    with open('/tmp/r.json', 'w') as cred:
        json.dump(data, cred)

t1 = PythonOperator(
    task_id='getprofile',
    provide_context=True,
    python_callable=getjson,
    dag=dag,
)


t1.doc_md = """\
#### Task Documentation
You can document your task using the attributes `doc_md` (markdown),
`doc` (plain text), `doc_rst`, `doc_json`, `doc_yaml` which gets
rendered in the UI's Task Instance Details page.
![img](http://montcs.bloomu.edu/~bobmon/Semesters/2012-01/491/import%20soul.png)
"""

dag.doc_md = __doc__

t2 = PythonOperator(
    task_id='getrating',
    provide_context=True,
    python_callable=getr,
    dag=dag,
)


t3 = JsonToMongoOperator(
    task_id='sendjson',
    provide_context=True,
    file_to_load="/tmp/p.json",
    mongoserver="db",
    mongopass="example",
    mongouser="root",
    dag=dag,
)


t4 = JsonToMongoOperator(
    task_id='sendrating',
    provide_context=True,
    file_to_load="/tmp/r.json",
    mongoserver="db",
    mongopass="example",
    mongouser="root",
    dag=dag,
)

t1 >> t2 >> t3 >> t4